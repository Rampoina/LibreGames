# Libre games

# What are they?

Libre games are fun games that are owned by the community.

# What are the advantages?

- Full conversion modding support.
- Everyone can improve them and give feedback.
- Community translations.
- No company can decide to discontinue them.
- Everyone can host their own server.
- Easy to make it available on multiple platforms.

# How is that achieved?

By making every asset of the game — such as code, music, art, 3d models — freely available, shareable and modifiable (always giving attribution to the original authors).

# Examples

## [Battle for Wesnoth](https://wesnoth.org/)

Battle for Wesnoth is a turn based strategy libre game.

![Wesnoth gameplay image](https://wesnoth.org/images/sshots/wesnoth-1.14.0-2.jpg)

### Mods:

Battle for Wesnoth has a wealth of [mods](https://www.wesnoth.org/addons/1.14/). Some of them drastically change the gameplay like:

- Elvish dinasty (Story based resource management game)
- Conquest (Risk like game)
- Bob's RPG (RPG game)
- Legend of the invincibles
- Cities of the frontier
- Den of thieves

Given that the art assets are freely available, shareable and modifiable they can be reused in completely different projects such as:

- [Arcmage](https://arcmage.org/#) (A card game)
- [Hale](https://sourceforge.net/projects/hale/) (A turn based RPG)

### Comunity translations

Battle for Wesnoth is translated to multiple [languages](https://wiki.wesnoth.org/WesnothTranslations) including minority languages like Basque that wouldn't get translated in non-libre games.

### Community development

The development of Battle for Wesnoth has been highly influenced by the community, especially in the early stages. There were many artists who contributed to the game, people that contributed new campaigns to it, etc. Much of that development happened on the main forums where people would propose and discuss the additions.

The game has been refined over the years thanks to the countless [contributors](https://wiki.wesnoth.org/Credits) and people who reported issues and gave feedback on the game.

### Platform support

Battle for Wesnoth is available on every main platform (Linux, MacOS, Windows, Android, iOS).
Not only that but it runs on homebrew consoles like the Pyra and Pandora.


## [Veloren](https://veloren.net)
## [Mindustry](https://mindustrygame.github.io/)
## [0ad](https://play0ad.com/)


# FAQ

## Why 'Libre'?
'Libre' means free as in freedom/liberty. It means that everyone has the freedom to access, modify and share the game. The game is free from any ownership: everyone (the community) owns it.
The term libre comes from [free software](https://www.gnu.org/philosophy/free-sw.html) and it was chosen instead of 'free' to avoid any confusion with "free as in cost".
## How can the game have a common vision if everyone can make any changes they wish?
Each game project has a main trusted community that makes the decisions. Being able to change the game locally doesn't mean that it will get accepted into the main project.
## If the games have a main community that controls them does that mean the game can't evolve outside of their vision?
No! Because the game is available to everyone, anyone is able to create a derivative project (called a fork).
## People can steal the game from you!
As with most Open Source project, you *have* to give proper attribution when you fork a game. In this way the community can benefit from the assets and enjoy the different project spawned from them.
